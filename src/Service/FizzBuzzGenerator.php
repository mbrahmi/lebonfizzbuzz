<?php


namespace App\Service;


class FizzBuzzGenerator
{
    /**
     * @param int $int1
     * @param int $int2
     * @param string $str1
     * @param string $str2
     * @param int $limit
     * @return array
     */
    public function generate(int $int1, int $int2, string $str1, string $str2, int $limit): array
    {
        $fizzbuzz = [];

        for ($i = 1; $i <= $limit; $i++){
            switch($i)
            {
                case ($i / $int1 == round($i / $int1) && $i / $int2 == round($i / $int2)):
                    $fizzbuzz[] = "$str1$str2";
                    break;
                case ($i / $int1 == round($i / $int1)):
                    $fizzbuzz[] = "$str1";
                    break;
                case ($i / $int2 == round($i / $int2)):
                    $fizzbuzz[] = "$str2";
                    break;
                default:
                    $fizzbuzz[] = "$i";
            }
        }

        return $fizzbuzz;
    }

}