<?php

namespace App\Entity;

use App\Repository\RequestRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RequestRepository::class)
 */
class Request
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $params;

    /**
     * @ORM\Column(type="integer")
     */
    private $hits;

    /**
     * @ORM\Version() @ORM\Column(type="datetime", nullable=true)
     */
    private $version;

    public function __construct()
    {
        $this->version = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getParams(): ?string
    {
        return $this->params;
    }

    /**
     * @param $params
     * @return $this
     */
    public function setParams($params): self
    {
        $this->params = $params;

        return $this;
    }

    public function getHits(): ?int
    {
        return $this->hits;
    }

    public function setHits(int $hits): self
    {
        $this->hits = $hits;

        return $this;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setVersion($version): self
    {
        $this->version = $version;

        return $this;
    }
}
