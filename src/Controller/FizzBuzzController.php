<?php

namespace App\Controller;

use App\Service\FizzBuzzGenerator;
use App\Validator\Validator;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\OptimisticLockException;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FizzBuzzController extends AbstractController
{
    /**
     * @Rest\Get(path="/fizzbuzz")
     * @param Request $request
     * @param FizzBuzzGenerator $generator
     * @param Validator $validator
     * @return Response
     * @throws \Exception
     */
    public function fizzbuzz(Request $request, FizzBuzzGenerator $generator, Validator $validator): Response
    {
        list($int1, $int2, $str1, $str2, $limit) = $this->validateParams($request, $validator);
        $fizzbuzz = $generator->generate($int1, $int2, $str1, $str2, $limit);

        try {
            $em = $this->getDoctrine()->getManager();
            $Request = $em->getRepository(\App\Entity\Request::class)->findOneBy([
                'params' => $request->getQueryString()
            ]);

            if ($Request) {
                $entity = $em->getRepository(\App\Entity\Request::class)
                    ->find($Request->getId(), LockMode::OPTIMISTIC, $Request->getVersion());

                $entity->setHits($entity->getHits()+1);
            } else {
                $entity = new \App\Entity\Request();
                $entity->setParams($request->getQueryString());
                $entity->setHits(1);
            }

            $em->persist($entity);
            $em->flush();
        } catch(OptimisticLockException $e) {
            return $this->json(
                ["message" => "Sorry, but someone else has already changed this entity. Please apply the changes again!"],
                Response::HTTP_NOT_FOUND
            );
        }

        return $this->json($fizzbuzz);
    }

    /**
     * @Rest\Get(path="/statistics")
     * @return JsonResponse
     */
    public function statistics(): Response
    {
        $em = $this->getDoctrine()->getManager();

            $Request = $em->getRepository(\App\Entity\Request::class)->findOneBy([],[
                'hits' => 'DESC'
            ]);
        return $this->json($Request);
    }

    /**
     * @param Request $request
     * @param Validator $validator
     * @return array
     * @throws \Exception
     */
    private function validateParams(Request $request, Validator $validator) : array {
        return [
            $validator->validateInt('int1', $request->query->get('int1')),
            $validator->validateInt('int2', $request->query->get('int2')),
            $validator->validateString('str1', $request->query->get('str1')),
            $validator->validateString('str2', $request->query->get('str2')),
            $validator->validateInt('limit', $request->query->get("limit"))
        ];
    }
}
