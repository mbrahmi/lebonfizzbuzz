<?php


namespace App\Validator;


class Validator
{
    /**
     * @param $name
     * @param $value
     * @param int $min
     * @param int $max
     * @return string|null
     * @throws \Exception
     */
    public function validateString($name, $value, $min = 1, $max = 255): ?string
    {
        $message = null;

        if (empty($value)){
            $message = $name.' Required.';
        } else if (strlen($value) < $min){
            $message = $name.' Too short.';
        } else if (strlen($value) > $max){
            $message = $name.' Too long.';
        }

        if ($message){
            throw new \Exception($message);
        }

        return $value;
    }

    /**
     * @param $name
     * @param $value
     * @return int|null
     * @throws \Exception
     */
    public function validateInt($name, $value): ?int
    {
        $message = null;
        if (!is_numeric($value)){
            $message = $name.' Must be integer.';
        } else if (1 > $value){
            $message = $name.' Must be greater than 0.';
        }

        if ($message){
            throw new \Exception($message);
        }

        return (int)$value;
    }
}