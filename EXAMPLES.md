#Examples:
After executing `make install`, run the following lines in your terminal or navigate to urls in your internet browser.

To see the most common FizzBuzz :
```bash
curl 'http://localhost:9090/statistics' | jq .
```

To generate FizzBuzz :
```bash
curl 'http://localhost:9090//fizzbuzz?int1=3&int2=5&str1=Fizz&str2=Buzz&limit=100' | jq .
```
