<?php

namespace App\Tests\Validator;

use App\Validator\Validator;
use PHPUnit\Framework\TestCase;

class ValidatorTest extends TestCase
{

    private $validator;

    protected function setUp(): void
    {
        $this->validator = New Validator();
    }

    /**
     * @param $int
     * @param $value
     * @param $expected
     * @throws \Exception
     * @dataProvider validateIntDataProvider
     */
    public function testValidateIntFail($int, $value, $expected)
    {
        $this->expectException($expected);
        $this->validator->validateInt($int, $value);
    }

    /**
     * @throws \Exception
     */
    public function testValidateIntSuccess()
    {
        $this->assertEquals(1, $this->validator->validateInt('int1',1));
        $this->assertEquals(2, $this->validator->validateInt('int2','2'));
    }

    public function validateIntDataProvider()
    {
        // test 1
        yield [
            'name' => 'int1',
            'value' => 'A',
            'expected' => \Exception::class
        ];
        // test 2
        yield [
            'name' => 'int1',
            'value' => 0,
            'expected' => \Exception::class
        ];
    }

    /**
     * @throws \Exception
     */
    public function testValidateStringSuccess()
    {
        $this->assertEquals('Fizz', $this->validator->validateString('str1','Fizz'));
        $this->assertEquals('Buzz', $this->validator->validateString('str2','Buzz'));
    }

    /**
     * @param $name
     * @param $value
     * @param $min
     * @param $max
     * @param $expected
     * @throws \Exception
     * @dataProvider validateStringDataProvider
     */
    public function testValidateStringFail($name, $value, $min, $max, $expected)
    {
        $this->expectException($expected);
        $this->validator->validateString($name, $value, $min, $max);
    }

    public function validateStringDataProvider()
    {
        // test 1
        yield [
            'name' => 'str1',
            'value' => '',
            'min' => 1,
            'max' => 255,
            'expected' => \Exception::class
        ];
        // test 2
        yield [
            'name' => 'str1',
            'value' => 'A',
            'min' => 2,
            'max' => 255,
            'expected' => \Exception::class
        ];
        // test 3
        yield [
            'name' => 'str1',
            'value' => 'AZEFAZERFAERF',
            'min' => 1,
            'max' => 5,
            'expected' => \Exception::class
        ];
    }
}
