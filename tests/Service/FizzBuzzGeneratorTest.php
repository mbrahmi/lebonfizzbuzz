<?php

namespace App\Tests\Service;

use App\Service\FizzBuzzGenerator;
use PHPUnit\Framework\TestCase;

class FizzBuzzGeneratorTest extends TestCase
{

    /**
     * @param int $int1
     * @param int $int2
     * @param string $str1
     * @param string $str2
     * @param int $limit
     * @param array $expected
     * @dataProvider fizzBuzzDataProvider
     */
    public function testGenerate(int $int1, int $int2, string $str1, string $str2, int $limit, array $expected)
    {
        $fizzBuzzGenerator = new FizzBuzzGenerator();
        $fizzBuzz = $fizzBuzzGenerator->generate($int1, $int2, $str1, $str2, $limit);
        $this->assertEquals($expected, $fizzBuzz);
    }

    public function fizzBuzzDataProvider()
    {
        // test 1
        yield [
            'int1' => 2,
            'int2' => 5,
            'str1' => 'Fizzz',
            'str2' => 'Buzzzzz',
            'limit' => 10,
            'expected' => [
                "1",
                "Fizzz",
                "3",
                "Fizzz",
                "Buzzzzz",
                "Fizzz",
                "7",
                "Fizzz",
                "9",
                "FizzzBuzzzzz"
            ],
        ];

        // test 2
        yield [
            'int1' => 3,
            'int2' => 4,
            'str1' => 'f',
            'str2' => 'b',
            'limit' => 12,
            'expected' => [
                "1",
                "2",
                "f",
                "b",
                "5",
                "f",
                "7",
                "b",
                "f",
                "10",
                "11",
                "fb"
            ],
        ];

        // test 3
        yield [
            'int1' => 1,
            'int2' => 4 ,
            'str1' => 'f',
            'str2' => 'b',
            'limit' => 12,
            'expected' => [
                "f",
                "f",
                "f",
                "fb",
                "f",
                "f",
                "f",
                "fb",
                "f",
                "f",
                "f",
                "fb"
            ],
        ];

        // test 4
        yield [
            'int1' => 13,
            'int2' => 2 ,
            'str1' => 'fizz',
            'str2' => 'buzz',
            'limit' => 12,
            'expected' => [
                "1",
                "buzz",
                "3",
                "buzz",
                "5",
                "buzz",
                "7",
                "buzz",
                "9",
                "buzz",
                "11",
                "buzz"
            ],
        ];
    }
}
