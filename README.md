# Lebonfizzbuzz
Lebonfizzbuzz is an API for generating FizzBuzz and knowing the most used one.

## Installation

The project installation is done with a Makefile. To be able to fully use the Makefile, you will need to have theses packages installed:
* docker
* docker-compose  

Then you can run `make install`.

## Usage
The `/fizzbuzz` endpoint accepts 5 parameters :
<br/>`str1` and `str2` are 2 strings (typically 'Fizz' and 'Buzz').
<br/>`int1` the multiples of this integer would be replaced by str1.
<br/>`int2` the multiples of this integer would be replaced by str2.
<br/>`limit` the limit of the FizzBuzz elements.

The `/statistics` endpoint shows the parameters and total hits of the most common FizzBuzz. 

See [Examples](EXAMPLES.md)

Execute  `make unit-test` command to run phpunit tests

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
