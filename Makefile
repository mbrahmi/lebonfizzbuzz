install: up composer-install database

up:
	@docker-compose up -d; \
	echo "\nServer is ready : http://localhost:9090"; \

composer-install:
	@docker-compose exec php-apache composer install --prefer-dist

database:
	@echo "Creating database ..."; \
	docker-compose exec php-apache bin/console doctrine:database:drop --if-exists --force --quiet; \
	docker-compose exec php-apache bin/console doctrine:database:create --quiet; \
	docker-compose exec php-apache bin/console doctrine:schema:create --quiet; \
	echo "Database created."

unit-test:
	@docker-compose exec php-apache php -d memory_limit=-1 bin/phpunit
